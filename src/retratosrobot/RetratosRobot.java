package retratosrobot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class RetratosRobot {
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
    String entrada = null;
    int opcion = 0;
    boolean seguir = true;
	
	ArrayList<String> elegido = new ArrayList<String>();

	String[] pelo = {
			"WWWWWWWWW", 
			"\\\\\\//////", 
			"|\"\"\"\"\"\"\"|",
			"| | | | |"
			};

	String[] ojos = {
			"| O   0 |",
			"|.(· ·)-|",
			"|-(o o)-|",
			"| \\   / |"
		};
	
	String[] nariz = {
			"@   J   @",
			"{   \"   }",
			"[   j   ]",
			"<   -   >"
	};
	
	String[] boca = {
			"|  ===  |",
			"|   -   |",
			"| ----- |",
			"| - - - |"
	};
	
	String[] barbilla = {
			"\\......./",
			"\\,,,,,,,/"
	};
	
	public void seleccionaRasgo(String nombre, String[] faccion) {
		System.out.println(nombre);
		for (int i = 0; i < faccion.length; i++) {
			System.out.println((i+1) + ". " + faccion[i]);
		}
		
		System.out.println("Selecciona un rasgo");
		try {
			entrada = reader.readLine();
			opcion = Integer.parseInt(entrada);
			elegido.add(faccion[(opcion-1)]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void generarRetrato() {
		seleccionaRasgo("PELO", pelo);
		seleccionaRasgo("OJOS", ojos);
		seleccionaRasgo("NARIZ", nariz);
		seleccionaRasgo("BOCA", boca);
		seleccionaRasgo("BARBILLA", barbilla);
	}
	
	public void mostrarRetrato() {
		for (String faccion : elegido) {
			System.out.println(faccion);
		}
		
		System.out.println("Si desea generar otro retrato pulse S");
		try {
			entrada = reader.readLine();
			if (entrada.equals("S")) {
				System.out.println("Generar otro retrato");
			} else {
				System.out.println("Salir de la aplicacion");
				seguir = false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		RetratosRobot rb = new RetratosRobot();
		do {
			rb.generarRetrato();
			rb.mostrarRetrato();
		} while (rb.seguir);
	}

}
