## nombre de la aplicación
<h1 align="center"> RETRATOS ROBOT</h1>

## Tabla de contenidos:
---

- [ciclo formativo]
- [módulo]
- [curso escolar]
- [autor]
- [descripción]

## ciclo formativo
Grado Superior de Aplicaciones Web
## módulo
Programación

## curso escolar
2019-2020

## autor
Roberto Llamedo Blanco

## descripción
Aplicación Java que permite crear retratos robot.
